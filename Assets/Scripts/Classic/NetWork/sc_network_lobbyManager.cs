﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class sc_network_lobbyManager : MonoBehaviourPunCallbacks
{
    public string gameVersion = "0.1";
    public Transform spawnPoint;
    public GameObject player;
    //sc_fps_controller sc_Fps_Controller;

    public int countCurrentPlayers = 0;
    public int countAllPlayers = 2;

    public int isPlayerMurder;

    RoomOptions roomOptions = new RoomOptions();
    

    public void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.ConnectUsingSettings();

        //sc_Fps_Controller = player.GetComponent<sc_fps_controller>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CreateRoom();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            JoinRoom();
        }
        if (Input.GetKeyDown(KeyCode.T)) TryCon();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected");
    }

    public void TryCon()
    {
        PhotonNetwork.NickName = "Player" + Random.Range(1, 100);

        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.ConnectUsingSettings();
    }

    public void CreateRoom()
    {
        roomOptions.MaxPlayers = 20;
        isPlayerMurder = Random.Range(1, countAllPlayers+1);
        PhotonNetwork.CreateRoom(null, roomOptions);

        Debug.Log("Room is created");
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.NickName = "Player" + Random.Range(1, 100);
        countCurrentPlayers++;
       // if (countCurrentPlayers == isPlayerMurder) { sc_Fps_Controller.isMurder = true; } else sc_Fps_Controller.isMurder = false;
        PhotonNetwork.Instantiate(player.name, spawnPoint.position, spawnPoint.rotation);
        Debug.Log("Player spawned");
    }
}
