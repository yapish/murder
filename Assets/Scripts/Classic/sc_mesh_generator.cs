using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sc_mesh_generator : MonoBehaviour {

	public int resolution;
	public float pointDist;
	public bool on;
	float[] anchorsLengths = new float [8];
	float t;
	Mesh mesh;
	bool aChanging;

	void Start () {
		MeshFilter meshFilter = GetComponent<MeshFilter> ();
		mesh = new Mesh ();
		meshFilter.mesh = mesh;
		for (int i = 0; i < 8; i++) {
			if ((i + 1) % 2 == 1) {
				if (i == 2 || i == 6) anchorsLengths[i] = Random.Range (1.8f, 3.5f);
				else anchorsLengths[i] = Random.Range (1.5f, 2.0f);
			}
			else anchorsLengths[i] = Random.Range (0.6f, 1.4f);
		}
	}
	Vector3 DotPosition (Vector3[] keyPoints) {
		return Mathf.Pow((1 - t), 3) * keyPoints[0] + Mathf.Pow((1 - t), 2) * keyPoints[1] * t * 3 + (1 - t) * keyPoints[2] * Mathf.Pow(t, 2) * 3 + keyPoints[3] * Mathf.Pow(t, 3);
	}
	void Update () {
		if (!on) {
			mesh.triangles = new int[0];
			return;
		}
		on = false;
		if (!aChanging) StartCoroutine (ChangeAnchors ());
		float forT = (float)1 / resolution;
		Vector3[] vert = new Vector3[(resolution + 2) * 8];
		vert[0] = new Vector3 (0, 0, 0);
		for (int el = 0; el < 8; el++) {
			t = 0;
			Vector3[] kPs = Points (el);
			int round = resolution * el;
			vert[1 + round] = kPs[0];
			for (int i = 2; i <= resolution; i++) {
				vert[i + round] = DotPosition (kPs);
				t += forT;
			}
		}
		int[] tri = new int[resolution * 24];
		for (int i = 0; i < resolution * 8; i++) {
			tri[i * 3] = 0;
			tri[i * 3 + 1] = i + 1;
			tri[i * 3 + 2] = i + 2;
		}
		tri[resolution * 24 - 1] = 1;

		mesh.vertices = vert;
		mesh.triangles = tri;
	}
	Vector3[] Points (int step) {
		Vector3[] points;
		switch (step) {
			case 0:
				points = new Vector3[4];
				points[0] = new Vector3 (0, anchorsLengths[0], 0);
				points[1] = new Vector3 (pointDist, anchorsLengths[0], 0);
				points[2] = new Vector3 (anchorsLengths[1] * 2 - pointDist * 0.7f, anchorsLengths[1]  + pointDist * 0.7f, 0);
				points[3] = new Vector3 (anchorsLengths[1] * 2, anchorsLengths[1], 0);
				return points;
			case 1:
				points = new Vector3[4];
				points[0] = new Vector3 (anchorsLengths[1] * 2, anchorsLengths[1], 0);
				points[1] = new Vector3 (anchorsLengths[1] * 2 + pointDist * 0.7f, anchorsLengths[1]  - pointDist * 0.7f, 0);
				points[2] = new Vector3 (anchorsLengths[2], pointDist, 0);
				points[3] = new Vector3 (anchorsLengths[2], 0, 0);
				return points;
			case 2:
				points = new Vector3[4];
				points[0] = new Vector3 (anchorsLengths[2], 0, 0);
				points[1] = new Vector3 (anchorsLengths[2], -pointDist, 0);
				points[2] = new Vector3 (anchorsLengths[3] * 2 + pointDist * 0.7f, -anchorsLengths[3]  + pointDist * 0.7f, 0);
				points[3] = new Vector3 (anchorsLengths[3] * 2, -anchorsLengths[3], 0);
				return points;
			case 3:
				points = new Vector3[4];
				points[0] = new Vector3 (anchorsLengths[3] * 2, -anchorsLengths[3], 0);
				points[1] = new Vector3 (anchorsLengths[3] * 2 - pointDist * 0.7f, -anchorsLengths[3]  - pointDist * 0.7f, 0);
				points[2] = new Vector3 (pointDist, -anchorsLengths[4], 0);
				points[3] = new Vector3 (0, -anchorsLengths[4], 0);
				return points;
			case 4:
				points = new Vector3[4];
				points[0] = new Vector3 (0, -anchorsLengths[4], 0);
				points[1] = new Vector3 (-pointDist, -anchorsLengths[4], 0);
				points[2] = new Vector3 (-anchorsLengths[5] * 2 + pointDist * 0.7f, -anchorsLengths[5]  - pointDist * 0.7f, 0);
				points[3] = new Vector3 (-anchorsLengths[5] * 2, -anchorsLengths[5], 0);
				return points;
			case 5:
				points = new Vector3[4];
				points[0] = new Vector3 (-anchorsLengths[5] * 2, -anchorsLengths[5], 0);
				points[1] = new Vector3 (-anchorsLengths[5] * 2 - pointDist * 0.7f, -anchorsLengths[5]  + pointDist * 0.7f, 0);
				points[2] = new Vector3 (-anchorsLengths[6], -pointDist, 0);
				points[3] = new Vector3 (-anchorsLengths[6], 0, 0);
				return points;
			case 6:
				points = new Vector3[4];
				points[0] = new Vector3 (-anchorsLengths[6], 0, 0);
				points[1] = new Vector3 (-anchorsLengths[6], pointDist, 0);
				points[2] = new Vector3 (-anchorsLengths[7] * 2 - pointDist * 0.7f, anchorsLengths[7]  - pointDist * 0.7f, 0);
				points[3] = new Vector3 (-anchorsLengths[7] * 2, anchorsLengths[7], 0);
				return points;
			case 7:
				points = new Vector3[4];
				points[0] = new Vector3 (-anchorsLengths[7] * 2, anchorsLengths[7], 0);
				points[1] = new Vector3 (-anchorsLengths[7] * 2 + pointDist * 0.7f, anchorsLengths[7]  + pointDist * 0.7f, 0);
				points[2] = new Vector3 (-pointDist, anchorsLengths[0], 0);
				points[3] = new Vector3 (0, anchorsLengths[0], 0);
				return points;
		}
		return new Vector3[4];
	}

	IEnumerator ChangeAnchors () {
		aChanging = true;
		float[] aLengthsForChange = new float[8];
		for (int i = 0; i < 8; i++) {
			if ((i + 1) % 2 == 1) {
				if (i == 2 || i == 6) aLengthsForChange[i] = (Random.Range (1.8f, 3.5f) - anchorsLengths[i]) / 20;
				else aLengthsForChange[i] = (Random.Range (1.5f, 2.0f) - anchorsLengths[i]) / 20;
			}
			else aLengthsForChange[i] = (Random.Range (0.6f, 1.4f) - anchorsLengths[i]) / 20;
		}
    	for (int ch = 0; ch < 20; ch++) {
    		for (int i = 0; i < 8; i++) {
				anchorsLengths[i] += aLengthsForChange[i];
			}
	    	yield return new WaitForSeconds (0.002f);
	    }
	    aChanging = false;
    }
}