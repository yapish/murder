using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("ControlSettings")]
public class SetSerialized {
   public SerializedMouse Mouse;
   [XmlArrayItem("Key",typeof(SerializedKey))]
   public List<SerializedKey> KeySet;

   public SetSerialized() {
   	Mouse = new SerializedMouse();
   	KeySet = new List<SerializedKey>();
   }

}

public class SerializedMouse
{
   public bool invertedMouse;
   public float mouseSensitivity;
}

public class SerializedKey
{
   [XmlAttribute("KeyCode")]
   public string keyCode;
}

public class sc_mainmenu_script : MonoBehaviour {
	public bool PauseMenu;
	bool createSelected, findSelected, settingsSelected, creditsSelected, profileSelected, exitSelected, yesSelected, noSelected;
	bool kmSelected, soundSelected, graphicsSelected, backToFutureSelected, applySelected, revertSelected;
	bool yesBackSelected, noBackSelected;
	bool settingsNotSaved;
	bool keyEntering, otrubaster;
	bool isWidthScreen, isHeightScreen, isResol;
	public byte selectedInSettings;
	public List <int> resolutions;
	int selectedResolution;
	int dotsForChoose;
	public Text createText, findText, settingsText, creditsText, profileText, exitText, yesText, noText;
	public Text kmText, soundText, graphicsText, backToFutureText, applyText, revertText;
	public Text yesBackText, noBackText;
	public Text wScreen, hScreen;
	public List <Text> resolScreen;
	public List <Text> ShadowsForSelect;
	public List <Text> ModelsForSelect;
	public List <Text> TexturesForSelect;
	public List <Text> ShadersForSelect;
	public List <Text> AntiAlisForSelect;
	public GameObject mainPanel, createPanel, findPanel, settingsPanel, creditsPanel, profilePanel, exitPanel;
	public GameObject kmPanel, soundPanel, graphicsPanel, warningPanel;
	public GameObject invrtButton, fScreenButton, vSynchButton, mBlurButton, multiCoreButton;
	public List <Image> Sliders;
	public List <Text> forEnterKey;
	public List <GameObject> dots;
	public RectTransform settingsList, gSettingsList, panelForAnchors, Canvas;
	public Color32 notSelectedColor, selectedColor, openedColor;
	public bool iMfl;
	public float mSfl;

	void ForSettingsStart () {
        LoadSettings ();
		int forWandH = GCD (Screen.width, Screen.height);
		wScreen.text = (Screen.width / forWandH).ToString ();
		hScreen.text = (Screen.height / forWandH).ToString ();
		resolScreen[1].text = Screen.width.ToString ();
		resolScreen[2].text = Screen.height.ToString ();
		Sliders[1].fillAmount = AudioListener.volume;
		if (Screen.fullScreen) fScreenButton.SetActive(true);
		else fScreenButton.SetActive(false);
		if (QualitySettings.vSyncCount == 0) {
			vSynchButton.SetActive (false);
		}
		else {
			vSynchButton.SetActive (true);
		}
	}

	void Update () {
		if (Input.GetAxis("Mouse ScrollWheel") != 0) {
			if (kmPanel.activeSelf) {
				settingsList.anchoredPosition -= new Vector2 (0, Input.GetAxis("Mouse ScrollWheel") * 440);
				if (settingsList.anchoredPosition.y < 0) settingsList.anchoredPosition -= new Vector2 (0, settingsList.anchoredPosition.y);
				if (settingsList.anchoredPosition.y > 220) settingsList.anchoredPosition -= new Vector2 (0, settingsList.anchoredPosition.y - 220);
			}
			else if (isWidthScreen) {
				settingsNotSaved = true;
				wScreen.text = (int.Parse(wScreen.text) + (int)(Input.GetAxis("Mouse ScrollWheel") * 10)).ToString();
				if (int.Parse(wScreen.text) < int.Parse(hScreen.text) + 1) hScreen.text = (int.Parse(wScreen.text) - 1).ToString();
				resolScreen[2].text = resolutions[selectedResolution].ToString ();
				resolScreen[1].text = ((int)(resolutions[selectedResolution] / int.Parse(hScreen.text) * int.Parse(wScreen.text))).ToString ();
			}
			else if (isHeightScreen) {
				settingsNotSaved = true;
				hScreen.text = (int.Parse(hScreen.text) + (int)(Input.GetAxis("Mouse ScrollWheel") * 10)).ToString();
				if (int.Parse(hScreen.text) < 2) hScreen.text = "2";
				if (int.Parse(wScreen.text) < int.Parse(hScreen.text) + 1) wScreen.text = (int.Parse(hScreen.text) + 1).ToString();
				resolScreen[2].text = resolutions[selectedResolution].ToString ();
				resolScreen[1].text = ((int)(resolutions[selectedResolution] / int.Parse(hScreen.text) * int.Parse(wScreen.text))).ToString ();
			}
			else if (isResol) {
				settingsNotSaved = true;
				selectedResolution += (int)(Input.GetAxis("Mouse ScrollWheel") * 10);
				if (selectedResolution < 0) selectedResolution = 0;
				if (selectedResolution > resolutions.Count - 1) selectedResolution = resolutions.Count - 1;
				resolScreen[2].text = resolutions[selectedResolution].ToString ();
				resolScreen[1].text = ((int)(resolutions[selectedResolution] / int.Parse(hScreen.text) * int.Parse(wScreen.text))).ToString ();
			}
			else if (graphicsPanel.activeSelf) {
				gSettingsList.anchoredPosition -= new Vector2 (0, Input.GetAxis("Mouse ScrollWheel") * 440);
				if (gSettingsList.anchoredPosition.y < 0) gSettingsList.anchoredPosition -= new Vector2 (0, gSettingsList.anchoredPosition.y);
				if (gSettingsList.anchoredPosition.y > 600) gSettingsList.anchoredPosition -= new Vector2 (0, gSettingsList.anchoredPosition.y - 600);
			}
		}
		if (otrubaster) {
			keyEntering = false;
			otrubaster = false;
			return;
		}
		if (keyEntering) return;
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			if (mainPanel.activeSelf) {
				if (createSelected) {
					createSelected = false;
					findSelected = true;
				}
				else if (findSelected) {
					findSelected = false;
					settingsSelected = true;
				}
				else if (settingsSelected) {
					settingsSelected = false;
					creditsSelected = true;
				}
				else if (creditsSelected) {
					creditsSelected = false;
//					profileSelected = true;
					exitSelected = true;
				}
				else if (profileSelected) {
					profileSelected = false;
					exitSelected = true;
				}
				else if (exitSelected) {
					exitSelected = false;
					createSelected = true;
				}
				else {
					createSelected = true;
				}
			}
			else if (settingsPanel.activeSelf) {
				if (applySelected || revertSelected) {
					applySelected = false;
					revertSelected = false;
					if (selectedInSettings == 1) {
						kmSelected = true;
					}
					else if (selectedInSettings == 2) {
						soundSelected = true;
					}
					else if (selectedInSettings == 3) {
						graphicsSelected = true;
					}
					else {
						kmSelected = true;
					}
				}
				else if (kmSelected) {
					kmSelected = false;
					soundSelected = true;
				}
				else if (soundSelected) {
					soundSelected = false;
					graphicsSelected = true;
				}
				else if (graphicsSelected) {
					graphicsSelected = false;
					backToFutureSelected = true;
				}
				else if (backToFutureSelected) {
					backToFutureSelected = false;
					kmSelected = true;
				}
				else {
					if (selectedInSettings == 1) {
						soundSelected = true;
					}
					else if (selectedInSettings == 2) {
						graphicsSelected = true;
					}
					else if (selectedInSettings == 3) {
						backToFutureSelected = true;
					}
					else {
						kmSelected = true;
					}
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			if (mainPanel.activeSelf) {
				if (createSelected) {
					createSelected = false;
					exitSelected = true;
				}
				else if (findSelected) {
					findSelected = false;
					createSelected = true;
				}
				else if (settingsSelected) {
					settingsSelected = false;
					findSelected = true;
				}
				else if (creditsSelected) {
					creditsSelected = false;
					settingsSelected = true;
				}
				else if (profileSelected) {
					profileSelected = false;
					creditsSelected = true;
				}
				else if (exitSelected) {
					exitSelected = false;
//					profileSelected = true;
					creditsSelected = true;
				}
				else {
					exitSelected = true;
				}
			}
			else if (settingsPanel.activeSelf) {
				if (applySelected || revertSelected) {
					applySelected = false;
					revertSelected = false;
					if (selectedInSettings == 1) {
						kmSelected = true;
					}
					else if (selectedInSettings == 2) {
						soundSelected = true;
					}
					else if (selectedInSettings == 3) {
						graphicsSelected = true;
					}
					else {
						backToFutureSelected = true;
					}
				}
				else if (kmSelected) {
					kmSelected = false;
					backToFutureSelected = true;
				}
				else if (soundSelected) {
					soundSelected = false;
					kmSelected = true;
				}
				else if (graphicsSelected) {
					graphicsSelected = false;
					soundSelected = true;
				}
				else if (backToFutureSelected) {
					backToFutureSelected = false;
					graphicsSelected = true;
				}
				else {
					if (selectedInSettings == 1) {
						backToFutureSelected = true;
					}
					else if (selectedInSettings == 2) {
						kmSelected = true;
					}
					else if (selectedInSettings == 3) {
						soundSelected = true;
					}
					else {
						backToFutureSelected = true;
					}
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			if (exitPanel.activeSelf) {
				if (yesSelected) {
					yesSelected = false;
					noSelected = true;
				}
				else if (noSelected) {
					noSelected = false;
					yesSelected = true;
				}
				else {
					yesSelected = true;
				}
			}
			if (warningPanel.activeSelf) {
				if (yesBackSelected) {
					yesBackSelected = false;
					noBackSelected = true;
				}
				else if (noBackSelected) {
					noBackSelected = false;
					yesBackSelected = true;
				}
				else {
					yesBackSelected = true;
				}
			}
			else if (settingsPanel.activeSelf) {
				if (applySelected) {
					applySelected = false;
					revertSelected = true;
				}
				else if (revertSelected) {
					revertSelected = false;
					if (selectedInSettings == 2) {
						soundSelected = true;
					}
					else if (selectedInSettings == 3) {
						graphicsSelected = true;
					}
					else {
						kmSelected = true;
					}
				}
				else {
					kmSelected = false;
					soundSelected = false;
					graphicsSelected = false;
					backToFutureSelected = false;
					applySelected = true;
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			if (exitPanel.activeSelf) {
				if (yesSelected) {
					yesSelected = false;
					noSelected = true;
				}
				else if (noSelected) {
					noSelected = false;
					yesSelected = true;
				}
				else {
					noSelected = true;
				}
			}
			if (warningPanel.activeSelf) {
				if (yesBackSelected) {
					yesBackSelected = false;
					noBackSelected = true;
				}
				else if (noBackSelected) {
					noBackSelected = false;
					yesBackSelected = true;
				}
				else {
					noBackSelected = true;
				}
			}
			else if (settingsPanel.activeSelf) {
				if (applySelected) {
					applySelected = false;
					if (selectedInSettings == 2) {
						soundSelected = true;
					}
					else if (selectedInSettings == 3) {
						graphicsSelected = true;
					}
					else {
						kmSelected = true;
					}
				}
				else if (revertSelected) {
					revertSelected = false;
					applySelected = true;
				}
				else {
					kmSelected = false;
					soundSelected = false;
					graphicsSelected = false;
					backToFutureSelected = false;
					revertSelected = true;
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.Return)) {
			if (createSelected) {
				createClick ();
			}
			else if (findSelected) {
				findClick ();
			}
			else if (settingsSelected) {
				settingsClick ();
			}
			else if (creditsSelected) {
				creditsClick ();
			}
			else if (profileSelected) {
				profileClick ();
			}
			else if (exitSelected) {
				exitClick ();
			}
			else if (yesSelected) {
				yesClick ();
			}
			else if (noSelected) {
				noClick ();
			}
			else if (kmSelected) {
				kmClick ();
			}
			else if (soundSelected) {
				soundClick ();
			}
			else if (graphicsSelected) {
				graphicsClick ();
			}
			else if (backToFutureSelected) {
				backTFClick ();
			}
			else if (applySelected) {
				applyClick ();
			}
			else if (revertSelected) {
				revertClick ();
			}
			else if (yesBackSelected) {
				yesBackClick ();
			}
			else if (noBackSelected) {
				noBackClick ();
			}
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (createPanel.activeSelf) {
				createPanel.SetActive (false);
				mainPanel.SetActive (true);
			}
			else if (findPanel.activeSelf) {
				findPanel.SetActive (false);
				mainPanel.SetActive (true);
			}
			else if (settingsPanel.activeSelf) {
				backTFClick ();
			}
			else if (creditsPanel.activeSelf) {
				creditsPanel.SetActive (false);
				mainPanel.SetActive (true);
			}
/*			else if (profilePanel.activeSelf) {
				profilePanel.SetActive (false);
				mainPanel.SetActive (true);
			}*/
			else if (exitPanel.activeSelf) {
				yesSelected = false;
				noSelected = false;
				exitPanel.SetActive (false);
				mainPanel.SetActive (true);
			}
			else if (warningPanel.activeSelf) {
				noBackClick ();
			}
			else if (!PauseMenu) {
				createSelected = false;
				findSelected = false;
				settingsSelected = false;
				creditsSelected = false;
				profileSelected = false;
				exitSelected = false;
				mainPanel.SetActive (false);
				exitPanel.SetActive (true);
			}
		}
		if (createSelected) {
			createText.color = selectedColor;
		}
		else {
			createText.color = notSelectedColor;
		}
		if (findSelected) {
			findText.color = selectedColor;
		}
		else {
			findText.color = notSelectedColor;
		}
		if (settingsSelected) {
			settingsText.color = selectedColor;
		}
		else {
			settingsText.color = notSelectedColor;
		}
		if (creditsSelected) {
			creditsText.color = selectedColor;
		}
		else {
			creditsText.color = notSelectedColor;
		}
/*		if (profileSelected) {
			profileText.color = selectedColor;
		}
		else {
			profileText.color = notSelectedColor;
		}*/
		if (exitSelected) {
			exitText.color = selectedColor;
		}
		else {
			exitText.color = notSelectedColor;
		}

		if (yesSelected) {
			yesText.color = selectedColor;
		}
		else {
			yesText.color = notSelectedColor;
		}
		if (noSelected) {
			noText.color = selectedColor;
		}
		else {
			noText.color = notSelectedColor;
		}

		if (kmSelected) {
			kmText.color = selectedColor;
		}
		else {
			if (selectedInSettings == 1) {
				kmText.color = openedColor;
			}
			else kmText.color = notSelectedColor;
		}
		if (soundSelected) {
			soundText.color = selectedColor;
		}
		else {
			if (selectedInSettings == 2) {
				soundText.color = openedColor;
			}
			else soundText.color = notSelectedColor;
		}
		if (graphicsSelected) {
			graphicsText.color = selectedColor;
		}
		else {
			if (selectedInSettings == 3) {
				graphicsText.color = openedColor;
			}
			else graphicsText.color = notSelectedColor;
		}
		if (backToFutureSelected) {
			backToFutureText.color = selectedColor;
		}
		else {
			backToFutureText.color = notSelectedColor;
		}

		if (applySelected) {
			applyText.color = selectedColor;
		}
		else {
			applyText.color = notSelectedColor;
		}
		if (revertSelected) {
			revertText.color = selectedColor;
		}
		else {
			revertText.color = notSelectedColor;
		}

		if (yesBackSelected) {
			yesBackText.color = selectedColor;
		}
		else {
			yesBackText.color = notSelectedColor;
		}
		if (noBackSelected) {
			noBackText.color = selectedColor;
		}
		else {
			noBackText.color = notSelectedColor;
		}
	}

	public void InCS () {
		findSelected = false;
		settingsSelected = false;
		creditsSelected = false;
		profileSelected = false;
		exitSelected = false;
		createSelected = true;
	}

	public void OutCS () {
		createSelected = false;
	}

	public void InFS () {
		createSelected = false;
		settingsSelected = false;
		creditsSelected = false;
		profileSelected = false;
		exitSelected = false;
		findSelected = true;
	}

	public void OutFS () {
		findSelected = false;
	}

	public void InSS () {
		createSelected = false;
		findSelected = false;
		creditsSelected = false;
		profileSelected = false;
		exitSelected = false;
		settingsSelected = true;
	}

	public void OutSS () {
		settingsSelected = false;
	}

	public void InCrdS () {
		createSelected = false;
		findSelected = false;
		settingsSelected = false;
		profileSelected = false;
		exitSelected = false;
		creditsSelected = true;
	}

	public void OutCrdS () {
		creditsSelected = false;
	}

	public void InPS () {
		createSelected = false;
		findSelected = false;
		settingsSelected = false;
		creditsSelected = false;
		exitSelected = false;
		profileSelected = true;
	}

	public void OutPS () {
		profileSelected = false;
	}

	public void InES () {
		createSelected = false;
		findSelected = false;
		settingsSelected = false;
		creditsSelected = false;
		profileSelected = false;
		exitSelected = true;
	}

	public void OutES () {
		exitSelected = false;
	}

	public void InYS () {
		noSelected = false;
		yesSelected = true;
	}

	public void OutYS () {
		yesSelected = false;
	}

	public void InNS () {
		yesSelected = false;
		noSelected = true;
	}

	public void OutNS () {
		noSelected = false;
	}

	public void InYBS () {
		noBackSelected = false;
		yesBackSelected = true;
	}

	public void OutYBS () {
		yesBackSelected = false;
	}

	public void InNBS () {
		yesBackSelected = false;
		noBackSelected = true;
	}

	public void OutNBS () {
		noBackSelected = false;
	}

	public void InKMS () {
		soundSelected = false;
		graphicsSelected = false;
		backToFutureSelected = false;
		applySelected = false;
		revertSelected = false;
		kmSelected = true;
	}

	public void OutKMS () {
		kmSelected = false;
	}

	public void InSndS () {
		kmSelected = false;
		graphicsSelected = false;
		backToFutureSelected = false;
		applySelected = false;
		revertSelected = false;
		soundSelected = true;
	}

	public void OutSndS () {
		soundSelected = false;
	}

	public void InGS () {
		kmSelected = false;
		soundSelected = false;
		backToFutureSelected = false;
		applySelected = false;
		revertSelected = false;
		graphicsSelected = true;
	}

	public void OutGS () {
		graphicsSelected = false;
	}

	public void InBTFS () {
		kmSelected = false;
		soundSelected = false;
		graphicsSelected = false;
		applySelected = false;
		revertSelected = false;
		backToFutureSelected = true;
	}

	public void OutBTFS () {
		backToFutureSelected = false;
	}

	public void InASs () {
		kmSelected = false;
		soundSelected = false;
		graphicsSelected = false;
		backToFutureSelected = false;
		revertSelected = false;
		applySelected = true;
	}

	public void OutASs () {
		applySelected = false;
	}

	public void InRVS () {
		kmSelected = false;
		soundSelected = false;
		graphicsSelected = false;
		backToFutureSelected = false;
		applySelected = false;
		revertSelected = true;
	}

	public void OutRVS () {
		revertSelected = false;
	}

	public void createClick () {
		mainPanel.SetActive (false);
		createPanel.SetActive (true);
		createSelected = false;
	}

	public void findClick () {
		mainPanel.SetActive (false);
		findPanel.SetActive (true);
		findSelected = false;
	}

	public void settingsClick () {
		mainPanel.SetActive (false);
		ForSettingsStart ();
		settingsPanel.SetActive (true);
		settingsSelected = false;
	}

	public void creditsClick () {
		mainPanel.SetActive (false);
		creditsPanel.SetActive (true);
		creditsSelected = false;
	}

	public void profileClick () {
/*		mainPanel.SetActive (false);
		profilePanel.SetActive (true);
		profileSelected = false;*/
	}

	public void exitClick () {
		mainPanel.SetActive (false);
		exitPanel.SetActive (true);
		exitSelected = false;
	}
	
	public void yesClick () {
		if (PauseMenu) SceneManager.LoadScene ("scn_main_menu", LoadSceneMode.Single);
		else Application.Quit ();
	}

	public void noClick () {
		exitPanel.SetActive (false);
		mainPanel.SetActive (true);
		noSelected = false;
	}

	public void kmClick () {
		if (keyEntering) return;
		selectedInSettings = 1;
		kmPanel.SetActive (true);
		settingsList.anchoredPosition -= new Vector2 (0, settingsList.anchoredPosition.y);
		soundPanel.SetActive (false);
		graphicsPanel.SetActive (false);
	}

	public void soundClick () {
		if (keyEntering) return;
		selectedInSettings = 2;
		soundPanel.SetActive (true);
		kmPanel.SetActive (false);
		graphicsPanel.SetActive (false);
	}

	public void graphicsClick () {
		if (keyEntering) return;
		selectedInSettings = 3;
		graphicsPanel.SetActive (true);
		kmPanel.SetActive (false);
		soundPanel.SetActive (false);
	}

	public void backTFClick () {
		if (keyEntering) return;
		kmSelected = false;
		soundSelected = false;
		graphicsSelected = false;
		backToFutureSelected = false;
		applySelected = false;
		revertSelected = false;
		settingsPanel.SetActive (false);
		if (settingsNotSaved) warningPanel.SetActive (true);
		else {
			kmSelected = false;
			soundSelected = false;
			graphicsSelected = false;
			backToFutureSelected = false;
			applySelected = false;
			revertSelected = false;
			yesBackSelected = false;
			kmClick ();
			mainPanel.SetActive (true);
		}
	}

	public void applyClick () {
		if (keyEntering) return;
		settingsNotSaved = false;
		SaveSettings ();
		Screen.SetResolution (int.Parse(resolScreen[1].text), int.Parse(resolScreen[2].text), fScreenButton.activeSelf);
		AudioListener.volume = Sliders[1].fillAmount;
		if (vSynchButton.activeSelf) QualitySettings.vSyncCount = 1;
		else QualitySettings.vSyncCount = 0;
	}

	public void revertClick () {
		if (keyEntering) return;
		settingsNotSaved = false;
		ForSettingsStart ();
	}

	public void yesBackClick () {
		settingsNotSaved = false;
		kmSelected = false;
		soundSelected = false;
		graphicsSelected = false;
		backToFutureSelected = false;
		applySelected = false;
		revertSelected = false;
		yesBackSelected = false;
		kmClick ();
		warningPanel.SetActive (false);
		mainPanel.SetActive (true);
	}

	public void noBackClick () {
		noBackSelected = false;
		yesBackSelected = false;
		warningPanel.SetActive (false);
		settingsPanel.SetActive (true);
	}

	public void InvertOn () {
		settingsNotSaved = true;
		if (keyEntering) return;
		invrtButton.SetActive (true);
	}

	public void InvertOff () {
		settingsNotSaved = true;
		if (keyEntering) return;
		invrtButton.SetActive (false);
	}

	public void SliderDown (int numchik) {
		settingsNotSaved = true;
		if (keyEntering) return;
		Sliders[numchik].fillAmount = (Input.mousePosition.x / Screen.width * Canvas.rect.width - (panelForAnchors.anchoredPosition.x + Canvas.rect.width * 0.5f) - 10) / 180;
	}

	public void SliderDrag (int numchik) {
		settingsNotSaved = true;
		if (keyEntering) return;
		Sliders[numchik].fillAmount = (Input.mousePosition.x / Screen.width * Canvas.rect.width - (panelForAnchors.anchoredPosition.x + Canvas.rect.width * 0.5f) - 10) / 180;
	}

	public void ButtonForChooseClick (int numchik) {
		if (keyEntering) return;
		keyEntering = true;
		dotsForChoose = numchik;
		forEnterKey[numchik].gameObject.SetActive(false);
		dots[numchik].SetActive(true);
	}

	void OnGUI () {
		if (keyEntering && !otrubaster)
			if(Event.current.isKey) {
				if (
					Event.current.keyCode == KeyCode.F1  ||
					Event.current.keyCode == KeyCode.F2  ||
					Event.current.keyCode == KeyCode.F3  ||
					Event.current.keyCode == KeyCode.F4  ||
					Event.current.keyCode == KeyCode.F5  ||
					Event.current.keyCode == KeyCode.F6  ||
					Event.current.keyCode == KeyCode.F7  ||
					Event.current.keyCode == KeyCode.F8  ||
					Event.current.keyCode == KeyCode.F9  ||
					Event.current.keyCode == KeyCode.F10 ||
					Event.current.keyCode == KeyCode.F11 ||
					Event.current.keyCode == KeyCode.F12
				) return;
				if (Event.current.keyCode != KeyCode.Escape) {
					settingsNotSaved = true;
					string buttonName = Event.current.keyCode.ToString();
					foreach (Text simularButton in forEnterKey)
						if (simularButton.text == Event.current.keyCode.ToString()) simularButton.text = "-";
					forEnterKey[dotsForChoose].text = buttonName;
				}
				dots[dotsForChoose].SetActive(false);
				forEnterKey[dotsForChoose].gameObject.SetActive(true);
				otrubaster = true;
			}
	}
	public void OnW () {
		isWidthScreen = true;
		wScreen.color = selectedColor;
	}
	public void OffW () {
		isWidthScreen = false;
		wScreen.color = notSelectedColor;
	}

	public void OnH () {
		isHeightScreen = true;
		hScreen.color = selectedColor;
	}

	public void OffH () {
		isHeightScreen = false;
		hScreen.color = notSelectedColor;
	}

	public void OnResol () {
		isResol = true;
		foreach (Text forResol in resolScreen) forResol.color = selectedColor;
	}

	public void OffResol () {
		isResol = false;
		foreach (Text forResol in resolScreen) forResol.color = notSelectedColor;
	}

	public void FullSOn () {
		settingsNotSaved = true;
		fScreenButton.SetActive (true);
	}

	public void FullSOff () {
		settingsNotSaved = true;
		fScreenButton.SetActive (false);
	}

	public void MultiCOn () {
		settingsNotSaved = true;
		multiCoreButton.SetActive (true);
	}

	public void MultiCOff () {
		settingsNotSaved = true;
		multiCoreButton.SetActive (false);
	}

	public void VSynchOn () {
		settingsNotSaved = true;
		QualitySettings.vSyncCount = 1;
		vSynchButton.SetActive (true);
	}

	public void VSynchOff () {
		settingsNotSaved = true;
		QualitySettings.vSyncCount = 0;
		vSynchButton.SetActive (false);
	}

	public void MotionBlurOn () {
		settingsNotSaved = true;
		mBlurButton.SetActive (true);
	}

	public void MotionBlurOff () {
		settingsNotSaved = true;
		mBlurButton.SetActive (false);
	}

	public void SelectShadows (int numerochek) {
		settingsNotSaved = true;
		foreach (Text notS in ShadowsForSelect) notS.color = notSelectedColor;
		ShadowsForSelect[numerochek].color = selectedColor;
	}

	public void SelectModels (int numerochek) {
		settingsNotSaved = true;
		foreach (Text notS in ModelsForSelect) notS.color = notSelectedColor;
		ModelsForSelect[numerochek].color = selectedColor;
	}

	public void SelectTextures (int numerochek) {
		settingsNotSaved = true;
		foreach (Text notS in TexturesForSelect) notS.color = notSelectedColor;
		TexturesForSelect[numerochek].color = selectedColor;
	}

	public void SelectShaders (int numerochek) {
		settingsNotSaved = true;
		foreach (Text notS in ShadersForSelect) notS.color = notSelectedColor;
		ShadersForSelect[numerochek].color = selectedColor;
	}

	public void SelectAntiAlis (int numerochek) {
		settingsNotSaved = true;
		foreach (Text notS in AntiAlisForSelect) notS.color = notSelectedColor;
		AntiAlisForSelect[numerochek].color = selectedColor;
	}

	public void findGClick () {
		SceneManager.LoadScene ("scn_test_bathroom", LoadSceneMode.Single);
	}

	static int GCD(int a, int b)
	{
	    while (b != 0)
	    {
	        var temp = b;
	        b = a % b;
	        a = temp;
	    }
	    return a;
	}

	private void SaveSettings () {
		SetSerialized keysFS = new SetSerialized ();
		keysFS.Mouse.invertedMouse = invrtButton.activeSelf;
		keysFS.Mouse.mouseSensitivity = Sliders[0].fillAmount;
		foreach (Text keyC in forEnterKey) {
			SerializedKey kes = new SerializedKey ();
			kes.keyCode = keyC.text;
			keysFS.KeySet.Add (kes);
		}
	    XmlSerializer serializer = new XmlSerializer(typeof(SetSerialized));
	    FileStream stream = new FileStream(Application.dataPath + @"/config.xml", FileMode.Create);
	    serializer.Serialize(stream, keysFS);
	    stream.Close();
	}

	public void LoadSettings() {
		SetSerialized keysFL = new SetSerialized ();
		XmlDocument doc = new XmlDocument();
		if (!System.IO.File.Exists(Application.dataPath + @"/config.xml")) SaveSettings();
		FileStream stream = File.OpenRead(Application.dataPath + @"/config.xml");
		byte[] array = new byte[stream.Length];
		stream.Read(array, 0, array.Length);
		doc.LoadXml(System.Text.Encoding.Default.GetString(array));
		XmlSerializer serializer = new XmlSerializer(typeof(SetSerialized));
		XmlReader reader = new XmlNodeReader(doc.ChildNodes[1]);
		keysFL = (SetSerialized)serializer.Deserialize(reader);

		iMfl = keysFL.Mouse.invertedMouse;
		mSfl = keysFL.Mouse.mouseSensitivity;
		invrtButton.SetActive(iMfl);
		Sliders[0].fillAmount = mSfl;
		for (int i = 0; i < forEnterKey.Count; i++) {
			forEnterKey[i].text = keysFL.KeySet[i].keyCode;
		}
		stream.Close();
    }
}