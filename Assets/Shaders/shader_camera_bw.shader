﻿Shader "Unlit/CameraBW"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _LerpValue ("LerpValue", Range(0,1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float _LerpValue;

            fixed4 frag (v2f_img IN) : COLOR
            {
                fixed4 sample = tex2D(_MainTex, IN.uv);

                float singleChannel = (sample.r *0.2126 + sample.g *0.7152 + sample.b *0.0722);
                float4 col = float4(singleChannel, singleChannel, singleChannel, 1);

                return lerp(sample, col, _LerpValue);
            }
            ENDCG
        }
    }
}
