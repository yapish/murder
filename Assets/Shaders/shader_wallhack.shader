Shader "WH" {
	Properties{
		_Color("Main Color", Color) = (.5,.5,.5,1)
	}
	SubShader{
		Tags { "RenderType" = "Opaque" "Queue" = "Transparent-2"}
		Blend SrcAlpha OneMinusSrcAlpha
		Color [_Color]
		Pass {
			ZWrite Off
            ZTest Always
			Stencil {
				Ref 2
				Comp always
				Pass replace
			}
		}
	}
}